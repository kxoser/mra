package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testRoverEncountersObstacleAtXY() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		assertTrue(marsRover.planetContainsObstacleAt(4, 7));

	}
	
	@Test
	public void testRoverNotEncounterObstacleAtXY() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		assertFalse(marsRover.planetContainsObstacleAt(5, 7));

	}
	
	@Test(expected = MarsRoverException.class) 
	public void testPlanetWithInvalidCoordinatesXY() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(-5,10,planetObstacles);
	
	}
	
	@Test
	public void testRoverReturnsLandingStatus() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,0,N)",marsRover.executeCommand("")); 

	}
	
	@Test
	public void testRoverTurnsRightFromStartLanding() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,0,E)",marsRover.executeCommand("r")); 

	}
	
	@Test
	public void testRoverTurnsLeftFromStartLanding() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,0,W)",marsRover.executeCommand("l")); 

	}
	
	@Test
	public void testRoverMovesFowardPhasingNord() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		assertEquals("(0,1,N)",marsRover.executeCommand("f")); 

	}
	
	@Test
	public void testRoverMovesFowardPhasingEst() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		marsRover.executeCommand("r");
		
		assertEquals("(1,0,E)",marsRover.executeCommand("f")); 

	}
	
	@Test
	public void testRoverMovesFowardPhasingWest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		marsRover.executeCommand("r");
		marsRover.executeCommand("f");
		marsRover.executeCommand("l");
		marsRover.executeCommand("l");
		
		assertEquals("(0,0,W)",marsRover.executeCommand("f")); 

	}
	
	@Test
	public void testRoverMovesFowardPhasingSud() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		marsRover.executeCommand("f");
		marsRover.executeCommand("l");
		marsRover.executeCommand("l");
		
		assertEquals("(0,0,S)",marsRover.executeCommand("f")); 

	}
	
	@Test
	public void testRoverMovesFowardPhasingWestFromRoverXEquals0() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		marsRover.executeCommand("l");
		
		assertEquals("(9,0,W)",marsRover.executeCommand("f")); 

	}
	
	@Test
	public void testRoverMovesFowardPhasingSudFromRoverYEquals0() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		marsRover.executeCommand("l");
		marsRover.executeCommand("l");
		
		assertEquals("(0,9,S)",marsRover.executeCommand("f")); 

	}
	
	@Test
	public void testRoverMovesFowardPhasingNordFromRoverYEqualsPlanetY() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(3,3,planetObstacles);
		
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		
		
		assertEquals("(0,0,N)",marsRover.executeCommand("f")); 
	}
	
	@Test
	public void testRoverMovesFowardPhasingEstFromRoverXEqualsPlanetX() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(3,3,planetObstacles);
		
		marsRover.executeCommand("r");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		marsRover.executeCommand("f");
		
		assertEquals("(0,0,E)",marsRover.executeCommand("f")); 
	}
	
	@Test
	public void testRoverMovesBackwardPhasingNord() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		marsRover.executeCommand("f");
		
		assertEquals("(0,0,N)",marsRover.executeCommand("b")); 

	}
	
	@Test
	public void testRoverMovesBackwardPhasingEst() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		marsRover.executeCommand("r");
		marsRover.executeCommand("f");
		
		assertEquals("(0,0,E)",marsRover.executeCommand("b")); 

	}
	
	@Test
	public void testRoverMovesBackwardPhasingWest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		marsRover.executeCommand("l");
		
		assertEquals("(1,0,W)",marsRover.executeCommand("b")); 

	}
	
	@Test
	public void testRoverMovesBackwardPhasingSud() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);
		
		marsRover.executeCommand("l");
		marsRover.executeCommand("l");
		
		assertEquals("(0,1,S)",marsRover.executeCommand("b")); 

	}
	
	@Test
	public void testRoverMovesWithMultipleCommands() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);

		assertEquals("(2,2,E)",marsRover.executeCommand("ffrff")); 

	}
	
	@Test
	public void testRoverWrappingToXPhasingNord() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);

		assertEquals("(0,9,N)",marsRover.executeCommand("b")); 

	}
	
	@Test
	public void testRoverWrappingToYPhasingEst() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(10,10,planetObstacles);

		assertEquals("(9,0,E)",marsRover.executeCommand("rb")); 

	}
	
	@Test
	public void testRoverWrappingToYPhasingWest() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(2,2,planetObstacles);

		assertEquals("(0,0,W)",marsRover.executeCommand("lbbb")); 

	}
	
	@Test
	public void testRoverWrappingToYPhasingSud() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(2,2,planetObstacles);

		assertEquals("(0,0,S)",marsRover.executeCommand("llbbb")); 

	}
	
	@Test
	public void testRoverMovingFowardEncounterAnObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>();
		MarsRover marsRover = new MarsRover(5,5,planetObstacles);

		planetObstacles.add("(2,2)");
		
		assertEquals("(1,2,E)(2,2)",marsRover.executeCommand("ffrfff")); 

	}
	
}
