package tdd.training.mra;

import java.util.List;

public class MarsRover {
	
	private int planetX;
	private int planetY;
	private int roverX;
	private int roverY;
	private String roverPhasing;
	private List<String> planetObstacles;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		
		if(planetX < 0 || planetY < 0) { throw new MarsRoverException(); }
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		
		roverX = 0;
		roverY = 0;
		roverPhasing = "N";
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {

		return planetObstacles.contains("("+x+","+y+")");
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		
		char character;
    	String obstacle = "";
    	
        for (int i = 0; i < commandString.length(); i++) {
        	character = commandString.charAt(i);
        	String commandChar = Character.toString(character);
        	
    		if(!commandChar.equals("")) {
    			
    			if(commandChar.equals("f")) {
					
				if(getRoverPhasing().equals("N")) { 
					if(getRoverY() == planetY) { setRoverY(-1); }
					
    				if(planetContainsObstacleAt(getRoverX(), getRoverY()+1)) {
    					int valY = getRoverY() +1;
    					obstacle = "("+getRoverX()+","+valY+")";
    				}else {
    					setRoverY(getRoverY()+1);
    				}
				}
				
				if(getRoverPhasing().equals("E")) { 
					if(getRoverX() == planetX) { setRoverX(-1); }
					
    				if(planetContainsObstacleAt(getRoverX()+1, getRoverY())) {
    					int valX = getRoverX() +1;
    					obstacle = "("+valX+","+getRoverY()+")";
    				}else {
    					setRoverX(getRoverX()+1);
    				}
					
				}
				
				if(getRoverPhasing().equals("W")) { 
					if(getRoverX() == 0) { setRoverX(planetX); }
					
					if(planetContainsObstacleAt(getRoverX()-1, getRoverY())) {
						int valX = getRoverX() -1;
    					obstacle = "("+valX+","+getRoverY()+")";
    				}else {
    					setRoverX(getRoverX()-1);
    				}
					
				 }
				
				if(getRoverPhasing().equals("S")) { 
					if(getRoverY() == 0) { setRoverY(planetY); }
					
					if(planetContainsObstacleAt(getRoverX(), getRoverY()-1)) {
						int valY = getRoverY() -1;
    					obstacle = "("+getRoverX()+","+valY+")";
    				}else {
    					setRoverY(getRoverY()-1);
    				}
					
				} 
    				
    				
    			}else {
    				
    				if(commandChar.equals("b")) {
    					
    					if(getRoverPhasing().equals("N")) { 
    						if(getRoverY() == 0) { setRoverY(planetY); }
    						setRoverY(getRoverY()-1); }
    					
    					if(getRoverPhasing().equals("E")) { 
    						if(getRoverX() == 0) { setRoverX(planetX); }
    						setRoverX(getRoverX()-1); }
    					
    					if(getRoverPhasing().equals("W")) { 
    						if(getRoverX() == planetX) { setRoverX(-1); }
    						setRoverX(getRoverX()+1); }
    					
    					if(getRoverPhasing().equals("S")) { 
    						if(getRoverY() == planetY) { setRoverY(-1); }
    						setRoverY(getRoverY()+1); } 
    					
    				}else {
    					setRoverPhasing(commandChar);
    					}
    				
    			}
    			
    			
    		}
        	
        }
		
		
		return getLandingStatus()+obstacle;
	}
	
	
	public String getLandingStatus() {
		return "("+roverX+","+roverY+","+roverPhasing+")";
	}
	
	public int getRoverX() {
		return roverX;
	}
	
	public void setRoverX(int newPosition) {
		roverX = newPosition;
	}
	
	public int getRoverY() {
		return roverY;
	}
	
	public void setRoverY(int newPosition) {
		roverY = newPosition;
	}
	
	public String getRoverPhasing() {
		return roverPhasing;
	}
	
	public void setRoverPhasing(String direction) {
			
		if(getRoverPhasing().equals("N")) {
			setRoverPhasingWhilePhaseNord(direction);
		}else {
			if(getRoverPhasing().equals("E")) {
				setRoverPhasingWhilePhaseEst(direction);
			}else {
				if(getRoverPhasing().equals("W")) {
					setRoverPhasingWhilePhaseWest(direction);
				}else {
					if(getRoverPhasing().equals("S")) {
						setRoverPhasingWhilePhaseSud(direction);
					}
				}
			}
		}
		

	}
	
	public void setRoverPhasingWhilePhaseNord(String direction) {
		if(direction.equals("r")) {
			roverPhasing = "E";
		}
		
		if(direction.equals("l")) {
			roverPhasing = "W";
		}
	}
	
	public void setRoverPhasingWhilePhaseEst(String direction) {
		if(direction.equals("r")) {
			roverPhasing = "S";
		}
		
		if(direction.equals("l")) {
			roverPhasing = "N";
		}
	}
	
	public void setRoverPhasingWhilePhaseWest(String direction) {
		if(direction.equals("r")) {
			roverPhasing = "N";
		}
		
		if(direction.equals("l")) {
			roverPhasing = "S";
		}
	}
	
	public void setRoverPhasingWhilePhaseSud(String direction) {
		if(direction.equals("r")) {
			roverPhasing = "W";
		}
		
		if(direction.equals("l")) {
			roverPhasing = "E";
		}
	}

}
